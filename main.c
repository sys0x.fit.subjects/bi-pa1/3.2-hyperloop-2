#ifndef __PROGTEST__
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#endif /* __PROGTEST__ */
typedef struct moznyHyperloop{
    unsigned int s1;
    unsigned int s2;
    unsigned int s3;
    unsigned int used;
}MOZNYHYPERLOOP;
unsigned long long int hyperloop2                          (unsigned long long int length,
                                                            unsigned int        s2,
                                                            unsigned int        s3,
                                                            unsigned int        bulkhead,
                                                            unsigned int      * c2,
                                                            unsigned int      * c3 ) {
    MOZNYHYPERLOOP h1, h2;
    h1.used = 0;
    h2.used = 0;
    int pocetKombinaci = 0, pocetS2 = 0, pocetS3 = 0, diffS3 = 100000, diffS2 = 100000, kolikratVejde;
    unsigned long long int x = 0;

    while (666) {

        while (666) {

            if (+pocetS2 * s2 + pocetS3 * s3 + (+pocetS2 + pocetS3 + 1) * bulkhead == length) {
                if (h1.used == 0) {

                    h1.s2 = pocetS2;
                    h1.s3 = pocetS3;
                    h1.used = 1;
                } else if (h2.used == 0) {

                    h2.s2 = pocetS2;
                    h2.s3 = pocetS3;
                    h2.used = 1;
                    diffS3 = h1.s3 - h2.s3;
                    diffS2 = h1.s2 - h2.s2;
                }

                if ((pocetS3 - diffS3) >= 0 && (pocetS2 - diffS2) >= 0 && h1.used == 1 && h2.used == 1 && diffS3 != 0) {
                    kolikratVejde = pocetS3 / diffS3 + 1;
                    pocetS3 = pocetS3 - diffS3 * kolikratVejde;
                    pocetS2 = pocetS2 - diffS2 * kolikratVejde;
                    pocetKombinaci = pocetKombinaci + kolikratVejde;
                    // printf("%d,%d\n,%d", pocetS2, pocetS3, kolikratVejde);

                } else {
                    // printf("%d,%d\n", pocetS2, pocetS3);
                    pocetKombinaci++;

                    *c2 = pocetS2;
                    *c3 = pocetS3;

                }


            } else if (pocetS2 * s2 + pocetS3 * s3 + (+pocetS2 + pocetS3) * bulkhead > length) {
                break;
            }
            if (s3 != 0 && s3 != s2 && pocetS3 == 0 && x == 0) {
                pocetS3 = (length - pocetS2 * s2 - (pocetS2) * bulkhead) / (s3 + bulkhead);
                if (pocetS3 <= 0) {
                    break;
                }
                x = 1;
            } else if (s3 != 0 && s3 != s2) {
                pocetS3++;
            } else break;
        }

        pocetS3 = 0;

        if (pocetS2 * s2 + pocetS3 * s3 + (pocetS2 + pocetS3 + 1) * bulkhead > length) {
            break;
        } else if (s2 != 0) {
            pocetS2++;
            x = 0;
        } else break;
    }

    return pocetKombinaci;
}



unsigned long long int hyperloop                           ( unsigned long long int length,
                                                              unsigned int        s1,
                                                              unsigned int        s2,
                                                              unsigned int        s3,
                                                              unsigned int        bulkhead,
                                                              unsigned int      * c1,
                                                              unsigned int      * c2,
                                                              unsigned int      * c3 ) {
    unsigned long long int pocetKombinaci=0;
    unsigned int pocetS1=0,x=0,c2save,c3save;

    c2save=*c2;
    c3save=*c3;
    if(s1==s2||s1==s3){
    s1=0;
    }if(length<=0){
        return pocetKombinaci;
    }
    while(666){

        pocetKombinaci=pocetKombinaci+hyperloop2((length-pocetS1*s1-bulkhead*pocetS1),s2,s3,bulkhead,c2,c3);
        //pocetS1 * s1+ (pocetS1+ 1) * bulkhead == length;
        if(pocetKombinaci!=0&&x==0){
            *c1=pocetS1;
            c2save=*c2;
            c3save=*c3;
            x=1;
        }
       /* if(pocetS1 * s1+ (pocetS1+ 1) * bulkhead == length){
            pocetKombinaci++;
        }*/
        pocetS1++;
        if (pocetS1 * s1+ (pocetS1+ 1) * bulkhead > length||s1==0){
            break;
        }





    }

    *c2=c2save;
    *c3=c3save;

    return pocetKombinaci;
}


#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
  unsigned int c1=0, c2=0, c3=0;
assert ( hyperloop ( 100, 0, 0, 0, 0, &c1, &c2, &c3 ) == 0 && 4 * c1 + 7 * c2 + 5 * c3 + 0 * ( c1 + c2 + c3 + 1 ) == 0 );
  assert ( hyperloop ( 12345, 8, 3, 11, 3, &c1, &c2, &c3 ) == 82708 && 8 * c1 + 3 * c2 + 11 * c3 + 3 * ( c1 + c2 + c3 + 1 ) == 12345 );
  c1 = 8;
    c2 = 9;
    c3 = 10;
  assert ( hyperloop ( 127, 12, 8, 10, 0, &c1, &c2, &c3 ) == 0&& c1 == 8&& c2 == 9&& c3 == 10 );
  assert ( hyperloop ( 127, 12, 8, 10, 3, &c1, &c2, &c3 ) == 4 && 12 * c1 + 8 * c2 + 10 * c3 + 3 * ( c1 + c2 + c3 + 1 ) == 127 );
  assert ( hyperloop ( 100, 35, 0, 0, 10, &c1, &c2, &c3 ) == 1
           && c2 == 0
           && c3 == 0
           && 35 * c1 + 10 * ( c1 + 1 ) == 100 );
  assert ( hyperloop ( 100, 35, 0, 35, 10, &c1, &c2, &c3 ) == 1
           && c2 == 0
           && 35 * c1 + 35 * c3 + 10 * ( c1 + c3 + 1 ) == 100 );
  assert ( hyperloop ( 100, 35, 35, 35, 10, &c1, &c2, &c3 ) == 1
           && 35 * c1 + 35 * c2 + 35 * c3 + 10 * ( c1 + c2 + c3 + 1 ) == 100 );

  return 0;
}
#endif /* __PROGTEST__ */
